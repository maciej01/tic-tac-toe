import pygame,   sys
from   pygame.locals import *

global BGCOLOR

GREEN = (0,   255,   0)
WHITE = (255, 255, 255)
RED   = (255,  0,   0 )
BLUE =  (0,   0,   255)
					  # Definitions!
FPS            = 15
ICONSIZE       = 64
WINDOWWIDTH    = (3*ICONSIZE)
WINDOWHEIGHT   = (3*ICONSIZE)
BGCOLOR        = WHITE
CROSS          = 'X'
CIRCLE         = 'O'

def main():
	pygame.init()
	pygame.display.set_caption('Tic Tac Toe')

	global FPSCLOCK, DISPLAYSURF, MENUSTAGE    # They are going to be used by functions too!

	DISPLAYSURF   = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
	FPSCLOCK      = pygame.time.Clock()
	MENUSTAGE     = "MENU"
	mouseX        = 0
	mouseY        = 0
	Loop          = True
	turn          = CROSS
	circleImage   = pygame.image.load('circle.png')
	crossImage    = pygame.image.load('cross.png')
	menuImage     = pygame.image.load('menu.png')
	gridImage     = pygame.image.load('grid.png')
	backImage     = pygame.image.load('background.png')
	crossWin      = pygame.image.load('crossWin.png')
	circleWin     = pygame.image.load('circleWin.png')
	stalemate     = pygame.image.load('stalemate.png')
	Map           = [["", "", ""], ["", "", ""], ["", "", ""]]

	DISPLAYSURF.fill(BGCOLOR)

	while True == Loop:    # Main Loop!

		DISPLAYSURF.fill(BGCOLOR)
		mouseClicked = False

		for event in pygame.event.get():    # Event Loop!

			if (event.type == QUIT) or (event.type == KEYUP and event.key == K_ESCAPE):
				pygame.quit()
				exit(0)

			elif event.type == MOUSEMOTION:
				mouseX, mouseY  =  event.pos

			elif event.type == MOUSEBUTTONUP:
				mouseX, mouseY  =  event.pos
				mouseClicked    =  True

		if (mouseClicked == True) and (MENUSTAGE == "GAME"):
			global BGCOLOR
			currentSquare = get_square(mouseX, mouseY, ICONSIZE)

			if currentSquare[0] != None:
				squareX = currentSquare[0]
				squareY = currentSquare[1]

				if Map[squareY][squareX] == "":
					if turn == CROSS:
						Map[squareY][squareX] = CROSS
						turn = CIRCLE

					elif turn == CIRCLE:
						Map[squareY][squareX] = CIRCLE
						turn = CROSS
			else:
				print "The square finding algorithm DIDN'T FIND SHIT!"

			win = check_victory(Map)

			if win == CROSS:
				print "Cross has won, congratulations!"
				Map = [["", "", ""], ["", "", ""], ["", "", ""]]
				BGCOLOR = RED
				MENUSTAGE = "CROSSWIN"

			elif win == CIRCLE:
				print "Circle has won, congratulations!"
				Map = [["", "", ""], ["", "", ""], ["", "", ""]]
				BGCOLOR = BLUE
				MENUSTAGE = "CIRCLEWIN"

			elif win == False:
				hasEmpty = False
				for XX in range(len(Map)):
					for YY in range(len(Map[XX])):
						if Map[XX][YY] == "":
							hasEmpty = True

				if hasEmpty == False:
					print "Tie - good luck on the next time!"
					Map = [["", "", ""], ["", "", ""], ["", "", ""]]
					BGCOLOR = GREEN
					MENUSTAGE = "STALEMATE"

		elif (mouseClicked == True) and (MENUSTAGE == "MENU"):
			pick = get_square(mouseX, mouseY, ICONSIZE)
			if pick == "GAME":
				MENUSTAGE = "GAME"
			elif pick == "EXIT":
				pygame.quit()
				exit(0)

		elif (mouseClicked == True) and ((MENUSTAGE == "CROSSWIN") or (MENUSTAGE == "CIRCLEWIN") or (MENUSTAGE == "STALEMATE")):
			pick = get_square(mouseX, mouseY, ICONSIZE)
			if pick == "CONTINUE":
				MENUSTAGE = "MENU"


		draw_map(Map, circleImage, crossImage, menuImage, gridImage, backImage, (crossWin, circleWin, stalemate), ICONSIZE)
		pygame.display.update()
		FPSCLOCK.tick(FPS)

def get_square(x, y, icon):
	global MENUSTAGE
	if MENUSTAGE == "GAME":
		for mouseX in range(3):
			for mouseY in range(3):
				localX = mouseX*icon
				localY = mouseY*icon
				if x <= localX+icon and x >= localX:
					if y <= localY+icon and y >= localY:
						return mouseX, mouseY

		return None, None
	elif MENUSTAGE == "MENU":
		half = (icon*3)/2
		if 0 <= y <= half:
			return "GAME"
		else:
			return "EXIT"
	elif (MENUSTAGE == "CROSSWIN") or (MENUSTAGE == "CIRCLEWIN") or (MENUSTAGE == "STALEMATE"):
		return "CONTINUE"

def draw_map(map, cirImage, crImage, mImage, grImage, backImage, winners, icon):
	global DISPLAYSURF, MENUSTAGE

	DISPLAYSURF.blit(backImage, (0, 0))
	if MENUSTAGE == "GAME":
		DISPLAYSURF.blit(grImage, (0, 0))
		for y in range(3):
			for x in range(3):
				localisation = (x*icon, y*icon)

				if map[y][x] == CROSS:
					DISPLAYSURF.blit(crImage, localisation)
				elif map[y][x] == CIRCLE:
					DISPLAYSURF.blit(cirImage, localisation)
				elif map[y][x] == "":
					temp = 0
	elif MENUSTAGE == "MENU":
		DISPLAYSURF.blit(mImage, (0, 0))
	elif MENUSTAGE == "STALEMATE":
		DISPLAYSURF.blit(winners[2], (0, 0))
	elif MENUSTAGE == "CIRCLEWIN":
		DISPLAYSURF.blit(winners[1], (0, 0))
	elif MENUSTAGE == "CROSSWIN":
		DISPLAYSURF.blit(winners[0], (0, 0))


def check_victory(board): # Ripped this code from user2032433 on stackoverflow. Thanks! :)
    combinations = [
        # horizontal
        ((0,0), (1,0), (2,0)),
        ((0,1), (1,1), (2,1)),
        ((0,2), (1,2), (2,2)),
        # vertical
        ((0,0), (0,1), (0,2)),
        ((1,0), (1,1), (1,2)),
        ((2,0), (2,1), (2,2)),
        # crossed
        ((0,0), (1,1), (2,2)),
        ((2,0), (1,1), (0,2))
    ]

    for coordinates in combinations:
        letters = [board[y][x] for x,y in coordinates]
        if len(set(letters)) == 1:
            return letters[0] # Returns corresponding letter for winner (X/O)

    return False

if __name__ == "__main__":

	main()